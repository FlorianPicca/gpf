### Version 3.2.1.1
- Correction d'un problème de calcul d'arrhes.

### Version 3.2.1
- Corrections mineures dans la fiche de réservation.

### Version 3.2
- Le tarif des séjours est toujours modifiable.
- Ajout nouveau logo dans la fiche de réservation.
- Changements graphiques avec nouvel emblème.

### Version 3.1.3.4
- Effectue jusqu'à 3 tentatives de sauvegarde sur le cloud en cas d'erreur.

### Version 3.1.3.3
- Corrections d'erreurs sur la fiche de réservation

### Version 3.1.3.2
- Modifications mineures de la fiche de réservation
- résolution d'un manque de prise en compte du titre Mme

### Version 3.1.3.1
- Modifications mineures de la fiche de réservation

### Version 3.1.3
- Nouvelle version de la fiche de réservation
- Possibilité de choisir la version depuis les réglages

### Version 3.1.2.1
- Modification fiche de réservation

### Version 3.1.2
- Ajout d'une nouvelle statistique.
- Correction de l'adresse sur la fiche de réservation.

### Version 3.1.1
- Correction d'un problème de synchronisation.

### Version 3.1
- Légères améliorations de performance.
- Suppression de fonctionnalités obsolettes.
- Le contrat de réservation affiche tous les numéros de téléphone.
- Correction d'un bug de recherche si le champ est vide.
- La barre de chargement s'adapte de nouveau à la taille de la fenêtre.
- Restructuration du code pour améliorer la stabilitée.
- Le serveur cloud a changé et ne sera plus compatible avec les anciennes versions.

### Version 3.0.1
- Correction bug de sauvegarde lorsqu'un sejour est ajouté ou modifié.

### Version 3.0
- Amélioration et restructuration de l'interface.
- Amélioration de la vitesse de recherche.
- Amélioration de la vitesse de sauvegarde.
- Ajouté la possibilité de préciser si une entrée/sortie aura lieu le matin ou l'après-midi.
- Gestion automatique des backups (tous les 3 jours, durée de vie 1 an).
- Suppression des alertes inutiles au démarrage.
- Le contrat de réservation affiche désormais uniquement le genre du client auquel il est adressé (Mr ou Mme).
- Diverses modifications de forme pour le contrat de réservation.
- Correction d'un bug faisant que l'âge du chat n'apparaissait pas sur le contrat de réservation.
- Correction d'un bug faisant que la date de départ était fausse au moment de l'édition si une heure était précisée.
- Correction d'un crash lors de l'affichage de la carte pour une adresse contenant des caractères spéciaux.
- Correction d'un crash lorsque la session de l'utilisateur était révoquée.
- Correction d'affichage de certaines cases sélectionnables.
- Désactivation de certaines fonctionnalités inutilisées.

### Version 2.9.8.6
- Installation automatique des futures mises à jour.

### Version 2.9.8.5
- Correction bug edition chat/sejour selectionne le dernier de la liste

### Version 2.9.8.4
- Correction bug prix par chat n'accepte pas les virgules.

### Version 2.9.8.3
- Changement mineur contrat de réservation.

### Version 2.9.8.2
- Changement mineur fiche de réservation.

### Version 2.9.8.1
- Compatible avec MacOS Catalina.
- Amélioration du message d'erreur de connexion.

### Version 2.9.8
- Compatible avec MacOS Catalina.
- Amélioration du message d'erreur de connexion.

### Version 2.9.7
- Correction d'un problème de calcul du séjour si une heure est spécifiée.

### Version 2.9.6
- Possibilité de modifier le prix du séjour si à cheval sur plusieurs saisons.

### Version 2.9.5
- Possibilité de modifier les ahrres
- Nouvelles statistiques disponibles
- Possibilité d'exporter les données des séjours au format CSV

### Version 2.9.4
- Abondon de google site pour stoquer les mises à jour. Passage à GitLab.

### Version 2.9.3
- Correction crash au démarrage.
- Correction bug d'affichage dans la fiche des séjours.

### Version 2.9.1
- Passage à MacOS High Sierra.

### Version 2.9.1
- Tentative de correction d'un bug de synchronisation.

### Version 2.9
- Ajout de vérifications de validité lors de la création des périodes de saison.
- Ajout d'un bouton permettant de vérifier qu'il n'y a pas de saisons à cheval et que la couverture est complète.
- Ajout de nouveaux filtres pour les recherches.
- Corrigé un problème empêchant de supprimer une saison ou une période.
- Corrigé un problème d'affichage lorsqu'il n'y a aucun client.
- Changé le style de la barre de navigation.

### Version 2.8.9
- Correction d’un problème faisant planter l'application lors des recherches.
- Ajout d'une barre de navigation horizontale pour simplifier l'accès aux différents menus.
- Suppression de l'ancien menu.

### Version 2.8.8
- Correction d’un problème faisant que le prix total du séjour n’été pas recalculé après modification des dates.
- Si l'application ne charge pas, une simple déconnexion depuis l'onglet "compte" résout le problème.

### Version 2.8.7
- Amélioration de la netteté de la signature.
Correction d’un problème faisant que le prix total du séjour n’était pas multiplié par le nombre de chats.
- Correction de l’accord du pluriel dans la fiche de réservation sur le détail du prix par chat.
- Correction de l’affichage du prix des séjours dans les détails du séjour.

### Version 2.8.6
- Modifications mineures dans la fiche de réservation.

### Version 2.8.5
- Correction d’un problème faisant que le prix total du séjour n’était pas sauvegardé.
- Ajout de la décomposition du prix journalier dans les fiches de reservation (En cas de séjour à cheval sur plusieurs périodes).

### Version 2.8.4
- Amélioration de l’affichage des heures d’arrivé et de départ dans la fiche de reservation.
- Ajout de saisons à spécifier dans les préférences.
- Les saisons permettent de définir des tarifs différent suivant les périodes de l’année.

### Version 2.8.3
- Les tarifs peuvent être des nombres à virgule.
- Ajout du regroupement des statistiques par jour/mois/année.

### Version 2.8.2
- Vous pouvez désormais préciser l'heure d'arrivée et de départ du client.
- Amélioration des performances pour les statistiques.

### Version 2.8.1
- Correction d’un problème.

### Version 2.8
- Correction d’un problème de calcul du nombre de jours dû aux changements d’heures.

### Version 2.7.9
- Ajout de la signature aux documents PDF.
- Ajouté la possibilité de ne pas faire payer les arrhes d’un séjour.
- Ajout d’alertes lors de la créations de doublons dans la base de données.
- Ajout de quelques astuces supplémentaires.
- Possibilité d’enlever la signature automatique dans les réglages.

### Version 2.7.8
- Changements dans l'affichage des détails des clients et des chats.

### Version 2.7.7
- Correction d'un bug avec la suppression d'un élément avec le clic droit de la souris.
- Les arrhes sont maintenant arrondies.
- Amélioré la fiabilité de la sauvegarde de vos données lors de la déconnexion.

### Version 2.7.6
- Ajout d'une case à cocher si on ne connait pas la date de naissance du chat.
- Les mises à jour se téléchargent automatiquement à l'ouverture de l'application.

### Version 2.7.5
- Vous pouvez désormais exporter une fiche de réservation d'un simple clic, plus besoin de la remplir à la main!
- L'âge a été remplacé par une date de naissance. Vous allez perdre l'âge de tous les chats.

### Version 2.7
- Restructuration interne de l'application afin d'améliorer sa stabilité.
- Correction de divers problèmes empêchant parfois l'utilisation normale de l'application.
- Ajout du drapeau français pour les clients français.
- Ajout d'astuces dans la fenêtre des outils.
- Ajout d'un bouton pour signaler un problème, dans la barre de menu.
- Les résultats de recherche sont classés par pertinence. Les résultats ayant la même pertinence sont classés par ordre alphabétique.
- Ajouté la possibilité d'indiquer avec qui un chat s'entend bien.
- Vous pouvez changer la couleur du graphique dans les réglages.
- Les valeurs des statistiques ont été corrigés.

### Version 2.6.2
- Correction d'un problème d'affichage dans le planning.
- Le planning prend désormais en compte les changements effectués directement.
- Correction d'un problème sur les vétérinaires.
- Les alertes apparaissent désormais dans l'après-midi.

### Version 2.6.1
- Correction d'un problème d'affichage dans le planning.
- Ajout d'un bouton "Retour" au planning lorsqu'on y accède à partir de la base de données.
- Ouverture au démarrage de la fenêtre des détails.
- Changé interface des réglages.
- Correction d'un problème d'affichage des dates sur les ordinateurs non Français.
- Correction d'un bug d'affichage lors du téléchargement de mises à jour.
- Correction d'un bug dans les remarques du chat.

### Version 2.6
- Planning de la pension! Vous pouvez voir l'occupation de votre pension pour une période donnée.
- Ajout d'une alerte un jour avant une entrée ou une sortie.
- Ajout du calcul du solde dans les outils.
- Correction de fautes d'orthographe.

### Version 2.5.2
- Correction d'un bug faisant crasher l'application lors de l'affichage des statistiques.

### Version 2.5.1
- Mise à jour nécessaire afin de conserver la synchronisation entre ordinateurs.